#include "back.h"
#include "y.tab.h"
#include <iostream>
extern int yylineno;
int count = 1;

std::string lowercase(std::string s)
{
	transform(s.begin(), s.end(), s.begin(), [](unsigned char c)
			  { return tolower(c); });
	return s;
}

void html_tag_validator(const char *char_attributes)
{
	std::string attributes = char_attributes;
	std::vector<std::string> attrs_list_duplicate;
	attributes.erase(remove(attributes.begin(), attributes.end(), ' '), attributes.end());
	attributes.erase(remove(attributes.begin(), attributes.end(), '\t'), attributes.end());

	int cur_offset = 0;
	int max_offset = attributes.size();
	while (cur_offset < max_offset - 1)
	{
		int eq_index = attributes.find("=", cur_offset);
		if (eq_index + 1 == attributes.size())
		{
			ParseError(std::string("Attribute '" + attributes + "' has no value"));
		}
		std::string attr_name = attributes.substr(cur_offset, eq_index - cur_offset);
		attr_name = lowercase(attr_name);
		if (find(attrs_list_duplicate.begin(), attrs_list_duplicate.end(), attr_name) != attrs_list_duplicate.end())
		{
			ParseError(std::string("Duplicate attribute '" + attr_name + "' in <html> tag"));
		}
		attrs_list_duplicate.push_back(attr_name);
		// if (find(TagsDB[std::string("html")].begin(), TagsDB[std::string("html")].end(), attr_name) == TagsDB[std::string("html")].end())
		// {
		// 	ParseError(std::string("Attribute '" + attr_name + "' can't be in tag <html>"));
		// }
		if (find(TagsDB[std::string("html")].begin(), TagsDB[std::string("html")].end(), attr_name) == TagsDB[std::string("html")].end())
		{
			std::regex attr_reg("data-.*");
			std::smatch match;
			if (std::regex_match(attr_name, match, attr_reg))
			{
				attr_name = "data-";
			}
			else if (find(global_attrs.begin(), global_attrs.end(), attr_name) == global_attrs.end())
			{
				ParseError(std::string("Attribute '" + attr_name + "' can't be in tag <html>"));
			}
			// else
			// {
			// 	ParseError(std::string("Attribute '" + attr_name + "' can't be in tag <" + name + ">"));
			// }
		}
		char quote;
		if (attributes[eq_index + 1] == '\'')
		{
			quote = '\'';
		}
		else if (attributes[eq_index + 1] == '\"')
		{
			quote = '\"';
		}
		else
		{
			ParseError(std::string("Attribute '" + attr_name + "' has no value"));
		}
		int quote_index = eq_index + 1;
		int end_quote_index = attributes.find(quote, quote_index + 1);
		std::string attr_value = attributes.substr(quote_index + 1, end_quote_index - quote_index - 1);
		attr_value = lowercase(attr_value);
		if (AttributesDB.find(attr_name) == AttributesDB.end())
		{
			ParseError(std::string("Oops) No attribute '" + attr_name + "' in attributes regexes database placed in tag <html>"));
		}
		std::regex attr_reg(AttributesDB[attr_name]);
		std::smatch match;
		if (!std::regex_match(attr_value, match, attr_reg))
		{
			ParseError(std::string("Invalid value '" + attr_value + "' for attribute '" + attr_name + "' in tag <html>"));
		}
		cur_offset = end_quote_index + 1;
	}
}
void ParseError(std::string text)
{
	printf("%s in line %i\n", text.c_str(), yylineno);
	exit(1);
}

void close_tag_function(const char *char_name)
{
	std::string name = char_name;
	name = lowercase(name);
	if (TagsDB.find(name) == TagsDB.end())
	{
		ParseError(std::string("Tag </" + name + "> does not exist"));
	}

	std::stack<std::string> current_stack(TagsStack);
	if (TagsStack.empty())
	{
		ParseError(std::string("Tag <" + name + "> is not opened!"));
	}
	while (!TagsStack.empty())
	{
		// если встречаем несовпадение закрывающего тега с последним "открытым",
		// то проверяем, не может ли быть этот последний открытый тег одиночным
		if (name != TagsStack.top() && (find(VariativeTags.begin(), VariativeTags.end(), TagsStack.top()) != VariativeTags.end()))
		{
			TagsStack.pop();
		}
		else if (name == TagsStack.top())
		{
			TagsStack.pop();
			break;
		}
		else
		{
			ParseError(std::string("Tag <" + name + "> is not closed!"));
		}
	}
}

bool location_tag_checks(std::string &name, std::stack<std::string> &current_stack)
{
	if (InHead)
	{
		// такой тег запрещен в head
		if (find(HeadTags.begin(), HeadTags.end(), name) == HeadTags.end())
		{
			ParseError(std::string("Tag <" + name + "> is not allowed in <head> tag"));
			return false;
		}
		if (name == "title")
		{
			TitleExist = true;
		}
	}
	else
	{
		if (name == "title")
		{
			ParseError(std::string("Tag <title> is not allowed in <body> tag"));
			return false;
		}
	}

	//нужно ли проверять вложенность
	if (NessInsideTags.find(name) != NessInsideTags.end())
	{
		std::string NessParentTag = NessInsideTags.find(name)->second.first;
		bool DirectEnclosure = NessInsideTags.find(name)->second.second;

		if (DirectEnclosure)
		{
			if (NessParentTag != current_stack.top())
			{
				ParseError(std::string("Tag <" + name + "> must be directly enclosured in " + NessParentTag + " tag"));
				return false;
			}
		}
		else
		{
			bool enclosure_is_ok = false;
			// идем по стеку, смотрим, есть ли нужная вложенность
			while (!current_stack.empty())
			{
				if (NessParentTag == current_stack.top())
				{
					enclosure_is_ok = true;
					break;
				}
				current_stack.pop();
			}
			if (!enclosure_is_ok)
			{
				ParseError(std::string("Tag <" + name + "> must be enclosured in " + NessParentTag + " tag"));
				return false;
			}
		}
	}
	return true;
}

void AddAttrTag(const char *char_name, const char *char_attributes, bool isSingleTag)
{
	std::string name = char_name;
	name = lowercase(name);
	if (TagsDB.find(name) == TagsDB.end())
	{
		ParseError(std::string("Tag <" + name + "> does not exist"));
	}
	std::string attributes = char_attributes;
	std::vector<std::string> attrs_list_duplicate;
	attributes.erase(remove(attributes.begin(), attributes.end(), ' '), attributes.end());
	attributes.erase(remove(attributes.begin(), attributes.end(), '\t'), attributes.end());
	int cur_offset = 0;
	int max_offset = attributes.size();
	while (cur_offset < max_offset - 1)
	{
		int eq_index = attributes.find("=", cur_offset);
		if (eq_index + 1 == attributes.size())
		{
			ParseError(std::string("Attribute '" + attributes + "' has no value"));
		}
		std::string attr_name = attributes.substr(cur_offset, eq_index - cur_offset);
		attr_name = lowercase(attr_name);
		if (find(attrs_list_duplicate.begin(), attrs_list_duplicate.end(), attr_name) != attrs_list_duplicate.end())
		{
			ParseError(std::string("Duplicate attribute '" + attr_name + "' in <" + name + "> tag"));
		}
		attrs_list_duplicate.push_back(attr_name);
		if (find(TagsDB[name].begin(), TagsDB[name].end(), attr_name) == TagsDB[name].end())
		{
			std::regex attr_reg("data-.*");
			std::smatch match;
			if (std::regex_match(attr_name, match, attr_reg))
			{
				// std::cout << attr_name << "\n";
				attr_name = "data-";
			}
			else if (find(global_attrs.begin(), global_attrs.end(), attr_name) == global_attrs.end())
			{
				ParseError(std::string("Attribute '" + attr_name + "' can't be in tag <" + name + ">"));
			}
			// else
			// {
			// 	ParseError(std::string("Attribute '" + attr_name + "' can't be in tag <" + name + ">"));
			// }
		}
		char quote;
		if (attributes[eq_index + 1] == '\'')
		{
			quote = '\'';
		}
		else if (attributes[eq_index + 1] == '\"')
		{
			quote = '\"';
		}
		else
		{
			ParseError(std::string("Attribute '" + attr_name + "' has no value"));
		}
		int quote_index = eq_index + 1;
		int end_quote_index = attributes.find(quote, quote_index + 1);
		std::string attr_value = attributes.substr(quote_index + 1, end_quote_index - quote_index - 1);
		attr_value = lowercase(attr_value);
		if (AttributesDB.find(attr_name) == AttributesDB.end())
		{
			ParseError(std::string("Oops) No attribute '" + attr_name + "' in attributes regexes database placed in tag <" + name + ">"));
		}
		std::regex attr_reg(AttributesDB[attr_name]);
		std::smatch match;
		if (!std::regex_match(attr_value, match, attr_reg))
		{
			ParseError(std::string("Invalid value '" + attr_value + "' for attribute '" + attr_name + "' in tag <" + name + ">"));
		}
		cur_offset = end_quote_index + 1;
	}
	std::stack<std::string>
		current_stack(TagsStack);

	// проверки на положение тега относительно ранее размещенных тегов
	location_tag_checks(name, current_stack);

	// проверки на одиночность тега (мб его не надо добавлять в стек и не надо ждать закрывающего)

	if (find(SingleTags.begin(), SingleTags.end(), name) == SingleTags.end())
	{
		if (isSingleTag)
		{
			if (find(VariativeTags.begin(), VariativeTags.end(), name) == VariativeTags.end())
			{
				ParseError(std::string("Tag <" + name + "> can't be single"));
			}
		}
		else
			TagsStack.push(name);
	}
}

void AddTag(const char *char_name)
{
	std::string name = char_name;
	name = lowercase(name);
	// std::cout << "Hadnling tag <" << name << "> №" << count << std::endl;
	// count++;

	if (TagsDB.find(name) == TagsDB.end())
	{
		ParseError(std::string("Tag <" + name + "> does not exist"));
	}
	std::stack<std::string> current_stack(TagsStack);

	// проверки на положение тега относительно ранее размещенных тегов
	location_tag_checks(name, current_stack);

	// проверки на одиночность тега (мб его не надо добавлять в стек и не надо ждать закрывающего)
	if (find(SingleTags.begin(), SingleTags.end(), name) == SingleTags.end())
	{
		TagsStack.push(name);
	}
}

void StackPush(const char *char_tag_name)
{
	TagsStack.push(char_tag_name);
}

void StackPop()
{
	TagsStack.pop();
}

void isStackEmpty()
{
	while (!TagsStack.empty() && TagsStack.top() != "head" && TagsStack.top() != "body")
	{
		if (find(VariativeTags.begin(), VariativeTags.end(), TagsStack.top()) == VariativeTags.end())
		{
			ParseError(std::string("Unpaired tag <" + TagsStack.top() + ">"));
		}
		else
		{
			TagsStack.pop();
		}
	}
}
