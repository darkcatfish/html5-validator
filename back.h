#ifndef _BACK_H_
#define _BACK_H_

#include "DB.h"
#include <stack>
#include <algorithm>
#include <algorithm>
#include <regex>

std::stack<std::string> TagsStack;
bool InHead = false, TitleExist = false;
void html_tag_validator(const char *attributes);
void AddTag(const char *char_name);
void AddAttrTag(const char *char_name, const char *char_attributes, bool isSingleTag = false);

void StackPush(const char *char_tag_name);
void StackPop();
void isStackEmpty();

void ParseError(std::string text);
void close_tag_function(const char *char_name);

#endif // _BACK_H_