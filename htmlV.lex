%option yylineno

%{
    #include "y.tab.h"
    #include <string.h>
    //extern int yylval;

	extern void yyerror(const char *s);
   
%}
%option noyywrap

%x xml_single_tag
%x xml_double_tag
%x style_single_tag
%x style_double_tag
%x script_single_tag
%x script_double_tag
%x comment
%x svg_single_tag
%x svg_double_tag
%%

(\"[^\"]*\")|(\'[^\']*\')   {strncpy(yylval.str, yytext, 2047); return QUOTE;};

"<script" {  BEGIN(script_single_tag);}
"<style"  {  BEGIN(style_single_tag);}
"<xml"  {  BEGIN(xml_single_tag);}
"<svg"  {  BEGIN(svg_single_tag);}
"<html"  {return HTML_OPEN;}
"</html>" {return HTML_CLOSE;}
"<head "  {return HEAD_OPEN;}
"<head>"  {return HEAD_OPEN_SINGLE;}
"</head>" {return HEAD_CLOSE;}
"<body " {return BODY_OPEN;}
"<body>" {return BODY_OPEN_SINGLE;}
"</body>" {return BODY_CLOSE;}
"<"  {  return LEFT_BRACKET; }
">"  {  return RIGHT_BRACKET; }
"!DOCTYPE html" { return DOCKTYPE_5; }
"!doctype html" { return DOCKTYPE_5; }
"="  {  return EQ;}
"/"  {  return SLASH;}

"<!--"("[")? { BEGIN(comment);}

[^<>/= \r\n\t]* { strncpy(yylval.str, yytext, 2047); /*printf("%s\n", yylval.str);*/ return TEXT;}
.|\r|\n {}

<svg_single_tag>
{
    "/>"        { BEGIN(INITIAL); }
    ">"	        { BEGIN(svg_double_tag); }
    .|\n		{ }
    <<EOF>>		{ yyerror("Unclosed <svg> tag"); }
}

<svg_double_tag>
{
    "</svg>"	{ BEGIN(INITIAL); }
    .|\n		{ }
    <<EOF>>		{ yyerror("Unclosed <xml> tag"); }
}

<xml_single_tag>
{
    "/>"        { BEGIN(INITIAL); }
    ">"	        { BEGIN(xml_double_tag); }
    .|\n		{ }
    <<EOF>>		{ yyerror("Unclosed <xml> tag"); }
}
<xml_double_tag>
{
    "</xml>"	{ BEGIN(INITIAL); }
    .|\n		{ }
    <<EOF>>		{ yyerror("Unclosed <xml> tag"); }
}

<style_single_tag>
{
    "/>"        { BEGIN(INITIAL); }
    ">"	        { BEGIN(style_double_tag); }
    .|\n	    { }
    <<EOF>>		{ yyerror("Unclosed <style> tag"); }
}

<style_double_tag>
{
    "</style>"	{ BEGIN(INITIAL); }
    .|\n		{ }
    <<EOF>>		{ yyerror("Unclosed <style> tag"); }
}

<script_single_tag>
{
    "/>"        { BEGIN(INITIAL); }
    ">"	        { BEGIN(script_double_tag); }
    .|\n		{ }
    <<EOF>>		{ yyerror("Unclosed <script> tag"); }
}

<script_double_tag>
{
    "</script>"	{ BEGIN(INITIAL); }
    .|\n		{ }
    <<EOF>>		{ yyerror("Unclosed <script> tag"); }
}

<comment>
{
    "-->"	    { BEGIN(INITIAL); }
    .|\n		{}
    <<EOF>>		{ yyerror("Unclosed comment");}
}

%%

