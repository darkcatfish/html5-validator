%{
    #include <stdio.h>
    #include <map>
    #include <string>
    #include <cstring>

    //extern std::string yylval;
	extern int yylex();
    extern int yylineno;

	extern void yyerror(const char *s)
	{
        printf("%s in line %i\n", s, yylineno);
        exit(0);
	}

    extern bool InHead, TitleExist;
    extern void StackPush(const char *char_tag_name);
    extern void StackPop();
    extern void AddTag(const char *char_name);
    extern void AddAttrTag(const char *char_name, const char *char_attributes, bool isSingleTag = false);
    extern void close_tag_function(const char *char_name);
    extern void isStackEmpty();
    extern void html_tag_validator(const char *attributes);
%}


%error-verbose

%union {
     //long long long_long_val;
     char str[4096];
     //char sym;
    }

%token<str> BODY_CLOSE BODY_OPEN TEXT LEFT_BRACKET RIGHT_BRACKET DOCKTYPE_5 EQ SLASH QUOTE HTML_OPEN HTML_CLOSE HEAD_OPEN HEAD_CLOSE HEAD_OPEN_SINGLE BODY_OPEN_SINGLE

%type <str> attribute attributes text_content

%%

main:  doctype html head_start content head_end body_start content body_end HTML_CLOSE
       | doctype html head_start content head_end body_start body_end HTML_CLOSE

html: HTML_OPEN RIGHT_BRACKET
      | HTML_OPEN attributes RIGHT_BRACKET {html_tag_validator($2);}

/* head_start: HEAD_OPEN {пушим в стек headStackPush("head"); InHead=true;} */
head_end: HEAD_CLOSE {/*попим из стека head*/isStackEmpty(); StackPop(); if (!TitleExist) {yyerror("No title tag in head!");} InHead=false;}

/* body_start: BODY_OPEN {StackPush("body");} */
body_end: BODY_CLOSE {isStackEmpty();}

head_start: HEAD_OPEN_SINGLE {StackPush("head"); InHead=true;}
            | HEAD_OPEN attributes RIGHT_BRACKET {InHead=true; AddAttrTag("head", $2);}

body_start: BODY_OPEN_SINGLE {StackPush("body");}
            | BODY_OPEN attributes RIGHT_BRACKET {AddAttrTag("body", $2);}

/* tag_body: LEFT_BRACKET TEXT RIGHT_BRACKET {AddTag($2);} //конструктор без атрибутов
    | LEFT_BRACKET TEXT attributes RIGHT_BRACKET {AddAttrTag($2, $3);}//конструктор с атрибутов
    | LEFT_BRACKET TEXT attributes SLASH RIGHT_BRACKET {AddAttrTag($2, $3, true);}//конструктор с атрибутов
    | LEFT_BRACKET SLASH TEXT RIGHT_BRACKET {close_tag_function($3);} //функция проверок на одиноч тег + изъятия из сетка */


doctype: LEFT_BRACKET DOCKTYPE_5 RIGHT_BRACKET

tag: LEFT_BRACKET TEXT RIGHT_BRACKET {AddTag($2);} //конструктор без атрибутов
    | LEFT_BRACKET TEXT SLASH RIGHT_BRACKET {AddTag($2);} //конструктор без атрибутов
    | LEFT_BRACKET TEXT attributes RIGHT_BRACKET {AddAttrTag($2, $3);}//конструктор с атрибутов
    | LEFT_BRACKET TEXT attributes SLASH RIGHT_BRACKET {AddAttrTag($2, $3, true);}//конструктор с атрибутов
    | LEFT_BRACKET SLASH TEXT RIGHT_BRACKET {close_tag_function($3);} //функция проверок на одиноч тег + изъятия из сетка

content: tag
	     | content tag
		 | text_content
		 | content text_content


text_content: EQ {memcpy($$, $1, 4096);}
		 | SLASH {memcpy($$, $1, 4096);}
		 | TEXT {memcpy($$, $1, 4096);}
		 | QUOTE {memcpy($$, $1, 4096);}

attributes: attribute {memcpy($$, $1, 4096);}
            | attributes attribute {strncat($1, $2, 2048); memcpy($$, $1, 4096);}

attribute: TEXT EQ QUOTE { strncat($1, "=", 2048); strncat($1, $3, 2048); strncat($1, " ", 1);  memcpy($$, $1, 4096);}
           | TEXT {strncat($1, "=", 2048); strncat($1, "'0'", 2048); strncat($1, " ", 1);  memcpy($$, $1, 4096);}
%%

int main()
{
    yyparse();
    puts("File validated accordintly to html5");
}


