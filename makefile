# .PHONY: all clean
# all: 
# 	yacc -d htmlV.y
# 	lex htmlV.lex
# 	$(CC) -std=c++11 lex.yy.c y.tab.c back.cpp -outfile htmlV.out

.PHONY: all clean linux windows
CC=g++
win_CC=.\mingw64\bin\c++.exe
win_bizon_path=.\flex+bison

all:
	$(info Please choose the OS: windows/linux)

linux: yacc_module_l cpp_module_l
	$(CC) -std=c++11 ym.o lm.o cm.o

windows: yacc_module_w cpp_module_w
	$(win_CC) -std=gnu++11 ym.o lm.o cm.o
yacc_module_l:
	yacc -d htmlV.y
	lex htmlV.lex
	$(CC) -std=c++11 -c -o ym.o y.tab.c 
	$(CC) -std=c++11 -c -o lm.o lex.yy.c


cpp_module_l:
	$(CC) -std=c++11 -w -c back.cpp -o cm.o

yacc_module_w:
	$(win_bizon_path)\win_bison.exe -d htmlV.y -o y.tab.c
	$(win_bizon_path)\win_flex.exe htmlV.lex
	$(win_CC) -std=gnu++11 -c -o ym.o y.tab.c 
	$(win_CC) -std=gnu++11 -c -o lm.o lex.yy.c


cpp_module_w:
	$(win_CC) -std=gnu++11 -w -c back.cpp -o cm.o



clean:
	rm -rf *.o lex.yy.c y.tab.c y.tab.h *.out *.exe
